## BRCA Datasets

In all of the following datasets, multiple probes corresponding to the same gene (Entrez ID) were collapsed using the 'MaxMean' method. `CollapseProbes` ([cRumbs](https://bitbucket.org/jeevb/crumbs "cRumbs")) is a wrapper around the `collapseRows` function from the [WGCNA](http://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/ "WGCNA") package, and is usable with `ExpressionSet` objects. Receptor status for each sample was defined as one of the following categories: `HER2` (HER2-positive), `ERPR` (ER or PR-positive), `TN` (triple-negative). Dataset-specific processing steps are as outlined below:

### TCGA
The pre-processed dataset is packaged [here](https://bitbucket.org/jeevb/tcga.brca "TCGA.BRCA"). Only primary tumor samples were retained.
```r
library(TCGA.BRCA)
data(tcga.brca)
tcga.brca <- tcga.brca[, pData(tcga.brca)$sample_type == 'Primary Tumor']
```

### ISPY1
The pre-processed dataset is packaged [here](https://bitbucket.org/jeevb/ispy1 "ISPY1"). One sample with invalid annotations was removed.
```r
library(ISPY1)
data(ispy1)
ispy1 <- CollapseProbes(ispy1, by = 'GENE')
ispy1 <- ispy1[, !is.na(as.character(pData(ispy1)$ispy.id))]
```

### GSE25066
The pre-processed dataset is packaged [here](https://bitbucket.org/jeevb/gse25066 "GSE25066"). ISPY samples were removed.
```r
library(GSE25066)
data(gse25066)
gse25066 <- CollapseProbes(gse25066, by = 'ENTREZ_GENE_ID')
gse25066 <- gse25066[, pData(gse25066)$source != 'ISPY']
```

### YauGeneExp
The pre-processed dataset is packaged [here](https://bitbucket.org/jeevb/yaugeneexp "YauGeneExp"). Samples with missing DMFS time and event data were removed.
```r
library(YauGeneExp)
data(yau)
yau <- CollapseProbes(yau, by = 'ENTREZ_ID')
yau <- yau[, !is.na(pData(yau)$e_dmfs) & !is.na(pData(yau)$t_dmfs)]
```

### ChinGeneExp
The pre-processed dataset is packaged [here](https://bitbucket.org/jeevb/chingeneexp "ChinGeneExp"). Samples with missing survival event data were removed. Categorical values for `erb_ihc` were modified to be consistent with those of `er` and `pr` respectively.
```r
library(ChinGeneExp)
data(chin)
chin <- CollapseProbes(chin, by = 'ENTREZ_GENE_ID')
chin <- chin[, !is.na(pData(chin)$disease_binary)]
pData(chin)$erb_ihc <- plyr::mapvalues(pData(chin)$erb_ihc,
                                       from = 0:1,
                                       to = c('Negative', 'Positive'))
```
